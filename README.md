Ansible role: vagrant
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-vagrant/badges/master/build.svg)

This role installs [HashiCorp Vagrant](https://www.vagrantup.com/).


Requirements
------------
gpg

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-vagrant.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-vagrant.git
 name: tcblome.vagrant
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-vagrant.git tcblome.vagrant
```

Role Variables
--------------
```yaml

# defaults file for ansible-role-vagrant
#
# vagrant_version	*	# version of vagrant to install

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.vagrant,
	         vagrant_version: "18.03.0~ce-0~ubuntu"
	        }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
